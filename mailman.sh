#!/bin/bash

echo "postfix postfix/mailname string my.hostname.com" | debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
echo "mailman mailman/site_languages multiselect 'en'" | debconf-set-selections

apt-get install mailutils -y
apt-get install postfix -y
apt-get install apache2 -y
apt-get install mailman -y
#echo "default_language: 'en'"