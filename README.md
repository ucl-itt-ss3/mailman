## WARNING, READ BEFORE USE!

# What is this?
This JSON file is a configuration file for PACKER.
PACKER is used for cloning existing VM Machines using a .vmx file.

# How to use?
After you finished installing PACKER, type `packer build <path\to\file.json>` into the cmd or shell.

There are three things what I noticed:
- Even if you are using windows, don't use `/`, ONLY `\`. You will get a string error if you do that.
- You must power off the existing VM, which you want to clone.
- You may have to change 2 lines in the SSH configuration file, which are:

```
/etc/ssh/sshd_config
PermitRootLogin no
PasswordAuthentication no
```

# Current status?
Everything is working properly, no bugs found.